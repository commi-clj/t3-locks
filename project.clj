(defproject cursive-lein "0.1.0-SNAPSHOT"
  :cljsbuild {
              :builds        {
                              :main {
                                     :compiler     {
                                                    ;:main          tk.labyrinth.test-runner
                                                    :optimizations :none
                                                    :output-to     "target/resources/public/js/main.js"
                                                    }
                                     :source-paths ["src/main"]
                                     }
                              :test {
                                     :compiler     {
                                                    :main          tk.labyrinth.test-runner
                                                    :optimizations :none
                                                    :output-to     "target/test/testable.js"
                                                    }
                                     :source-paths ["src/main", "src/test"]
                                     }
                              }
              :test-commands {"unit" ["phantomjs" "target/test/testable.js" "..."]}
              ;:test-commands {"tests" ["node" "phantom/unit-test.js" "..."]}
              }
  :dependencies [
                 [cljs-css-modules "0.2.1"]
                 [lein-cljsbuild "1.1.7"]
                 [lein-doo "0.1.10"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/clojurescript "1.10.520"]
                 [reagent "0.8.1"]
                 [thheller/shadow-cljs "2.8.42"]
                 ]
  :plugins [
            [lein-cljsbuild "1.1.7"]
            [lein-doo "0.1.10"]
            ]
  :source-paths ["src/main"]
  :test-paths ["src/test"]
  ;:description "FIXME: write description"
  ;:url "http://example.com/FIXME"
  ;:license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
  ;          :url  "https://www.eclipse.org/legal/epl-2.0/"}
  ;:repl-options {:init-ns cursive-lein.core}
  )
