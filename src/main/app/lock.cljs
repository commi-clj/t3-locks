(ns app.lock
  (:require
    [app.lock.component.settings :as lock-settings-c]
    [app.lock.lock-logic :as logic]
    [app.style]
    [reagent.core :as r]
    [tk.labyrinth.lock.ui-logic :as ui-logic]
    [tk.labyrinth.util.math-js :as math]
    ))

(defn layer-state [] (math/random-between 0 360))

(defonce state (r/atom {
                        :lock          {
                                        :layer-angles    [
                                                          (layer-state)
                                                          (layer-state)
                                                          (layer-state)
                                                          (layer-state)
                                                          (layer-state)
                                                          (layer-state)
                                                          ]
                                        :pick-angle      {
                                                          :modifiers {}
                                                          :pointed   0
                                                          }
                                        :unlock-progress 0
                                        }
                        :lock-settings {
                                        :layer-number 4
                                        :complexity   0     ; 4 8 16 any
                                        }
                        :test          true
                        }))

(defn root-property [state] {
                             :get    @state
                             :set    (fn [next] (reset! state next))
                             :update (fn [reducer] (swap! state reducer))
                             })

(defn property [parent-property property-name] {
                                                :get    (get (:get parent-property) property-name)
                                                :set    (fn [next] ((:set parent-property) (assoc (:get parent-property) property-name next)))
                                                :update (fn [reducer] ((:update parent-property) (fn [previous] (assoc previous property-name (reducer (get previous property-name))))))
                                                })

(defn layer-component [layer-angle pick-angle locked target size]
  (let [
        hot (and target (<= (math/angle-diff pick-angle layer-angle) 10))
        ]
    [:div {
           :class [
                   (:layer app.style/style-lock)
                   (when locked "locked")
                   (when target "target")
                   (when hot "hot")
                   ]
           :style {
                   :height    size
                   :transform (str "rotate(" (if locked (- layer-angle) 0) "deg)")
                   :width     size
                   }
           }]
    )
  )

(defn pick-component [pick-angle]
  (let [
        base 100
        start base
        mid (* base 1.5)
        offset 2
        end (* base 2)
        size (* base 2.1)
        ]
    [:svg {
           :class        (:pick app.style/style-lock)
           :fill         "none"
           :stroke       "black"
           :stroke-width 2
           :style        {
                          :height    size
                          :transform (str "rotate(" (- -45 pick-angle) "deg)")
                          :width     size
                          }
           }
     [:polygon {:points (str
                          start "," start " "
                          (+ mid offset) "," (- mid offset) " "
                          end "," end " "
                          (- mid offset) "," (+ mid offset)
                          )}]
     ]
    )
  )

(defn on-lock-blur! [pick-angle-modifiers-p]
  ((:set pick-angle-modifiers-p) {})
  )

(defn on-lock-key-change! [pick-angle-modifiers-p event down]
  (if (not (.-repeat event)) (let [
                                   _ ((:set pick-angle-modifiers-p) (logic/calculate-modifiers (:get pick-angle-modifiers-p) (.-keyCode event) down))
                                   ; TODO: Process space
                                   ] nil))
  )

(defn lock-component [lock-p]
  (let [
        layer-angles-v (:layer-angles (:get lock-p))
        pick-angle-p (property lock-p :pick-angle)
        pick-angle-modifiers-p (property pick-angle-p :modifiers)
        pick-angle-pointed-p (property pick-angle-p :pointed)
        calculated-pick-angle (logic/calculate-pick-angle (:get pick-angle-p))
        unlock-progress-p (property lock-p :unlock-progress)
        unlock-progress-v (:get unlock-progress-p)
        ]
    [:div {
           :class         (:lock app.style/style-lock)
           :on-blur       #(on-lock-blur! pick-angle-modifiers-p)
           :on-click      #((:set unlock-progress-p) (logic/next-unlock-progress unlock-progress-v calculated-pick-angle layer-angles-v))
           :on-key-down   #(on-lock-key-change! pick-angle-modifiers-p % true)
           :on-key-up     #(on-lock-key-change! pick-angle-modifiers-p % false)
           :on-mouse-move #((:set pick-angle-pointed-p) (ui-logic/calc-angle %))
           :tabindex      0
           }
     ; FIXME: Replace with non-barrier conversion
     (for [[index layer-angle] (map-indexed vector layer-angles-v)]
       ^{:key index} [layer-component
                      layer-angle
                      calculated-pick-angle
                      (>= index unlock-progress-v)
                      (= index unlock-progress-v)
                      (+ 90 (* (count layer-angles-v) 10) (- (* index 15)))
                      ]
       )
     (pick-component calculated-pick-angle)
     ]
    )
  )

(defn lock-wrapper-component []
  (let [
        root-property (root-property state)
        lock-p (property root-property :lock)
        _ (:set (property root-property :test) 0)
        ]
    [:<>
     [:button {
               :on-click #()
               } "TEST: " (:test (:get root-property))]
     (lock-settings-c/root (property root-property :lock-settings) (:set lock-p))
     (lock-component lock-p)
     ]
    )
  )
