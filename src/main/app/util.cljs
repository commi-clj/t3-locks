(ns app.util)

(extend-type js/HTMLCollection
						 ISeqable
						 (-seq [array] (array-seq array 0)))
