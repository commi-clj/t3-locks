(ns app.style
  (:require [cljs-css-modules.macro :refer-macros [defstyle]])
  )

(defstyle style-default
          ["body" {
                   :height "100%"
                   :margin "0px"
                   }]
          )

(defstyle style-root
          ["#root" {
                    :display        "flex"
                    :flex-direction "column"
                    :height         "100%"
                    }]
          )

(defstyle style-lock
          [".lock" {
                    :background-color "lightgrey"
                    :display          "grid"
                    :flex-grow        "1"
                    :outline          "none"
                    :place-items      "center center"
                    }]
          [".lock > *" {
                        :grid-column "1"
                        :grid-row    "1"
                        }]
          [".layer" {
                     :border              "thick solid"
                     :border-color        "darkblue transparent darkblue darkblue"
                     :border-radius       "100%"
                     :transition-duration "1s"
                     :transition-property "transform"
                     }]
          [".layer.locked" {
                            ;:border-color "darkblue darkblue darkblue darkblue"
                            :border-color "darkblue blue darkblue darkblue"
                            }]
          [".layer.locked.target" {
                                   ;:border-color        "darkblue darkblue darkblue darkblue"
                                   :border-color        "darkblue cyan darkblue darkblue"
                                   :transition-duration "2s, 1s"
                                   :transition-property "border-color, transform"
                                   }]
          [".layer.locked.target.hot" {
                                       :border-color        "red red red red"
                                       :transition-duration "2s"
                                       :transition-property "border-color"
                                       }]
          )
