(ns app.main
  (:require [app.lock])
  )

(defn init []
  (js/console.log "Initializing at: " (js/Date.now))
  (reagent.core/render [app.lock/lock-wrapper-component] (js/document.getElementById "root"))
  )

(defn main [] (init))

(defn reload [] (init))
