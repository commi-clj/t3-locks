(ns tk.labyrinth.lock.ui-logic
  (:require
    [tk.labyrinth.util.rectangle :as rectangle]
    [tk.labyrinth.util.math-js :as math]
    ))

(defn angle-between-points [start end]
  (math/rad-to-deg (math/atan2
                     (- (:y end) (:y start))
                     (- (:x end) (:x start)))))

(defn element-to-rectangle [element]
  (let [box (.getBoundingClientRect element)]
    (rectangle/rectangle
      (.-x box)
      (- 0 (.-y box) (.-height box))
      (.-width box)
      (.-height box))))

(defn event-to-point [event]
  {:x (.-clientX event) :y (- (.-clientY event))})

(defn calc-angle [event]
  (angle-between-points
    (rectangle/centre (element-to-rectangle (.-currentTarget event)))
    (event-to-point event)))
