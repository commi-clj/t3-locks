(ns tk.labyrinth.lock.logic
  (:require [tk.labyrinth.util.math-js :as math])
  )

(defn angle-modifier [keyCode]
  (case keyCode
    37 [:left [-1 0]]
    38 [:up [0 1]]
    39 [:right [1 0]]
    40 [:down [0 -1]]
    65 [:a [-1 0]]
    68 [:d [1 0]]
    83 [:s [0 -1]]
    87 [:w [0 1]]
    97 [:num1 [(- (math/projection-45)) (- (math/projection-45))]]
    98 [:num2 [0 -1]]
    99 [:num3 [(math/projection-45) (- (math/projection-45))]]
    100 [:num4 [-1 0]]
    102 [:num6 [1 0]]
    103 [:num7 [(- (math/projection-45)) (math/projection-45)]]
    104 [:num8 [0 1]]
    105 [:num9 [(math/projection-45) (math/projection-45)]]
    nil
    ))

(defn calculate-modifiers [modifiers keyCode add]
  (let [
        modifier (angle-modifier keyCode)
        modifier-id (first modifier)
        modifier-value (second modifier)
        ]
    (if (some? modifier)
      (if add
        (assoc modifiers modifier-id modifier-value)
        (select-keys modifiers (for [[k v] modifiers :when (not= k modifier-id)] k)))
      modifiers)
    ))

(defn calculate-modified-angle [modifiers]
  (let [
        reduced (reduce (fn [[x0 y0] [x1 y1]] [(+ x0 x1) (+ y0 y1)]) [0 0] (map val modifiers))
        ]
    reduced
    )
  )

(defn create-lock [lock-settings]

  )

(defn calculate-pick-angle [pick-angle]
  (let [
        modifiers-vector (calculate-modified-angle (:modifiers pick-angle))
        modifiers-angle (math/rad-to-deg (Math/atan2 (second modifiers-vector) (first modifiers-vector)))
        ]
    (if (not= modifiers-vector [0 0]) modifiers-angle (:pointed pick-angle))
    )
  )

(defn pick-matches [pick-angle target-angle]
  (<= (math/angle-diff pick-angle target-angle) 10)
  )

(defn target-angle [unlock-progress layer-angles] (nth layer-angles unlock-progress nil))

(defn next-unlock-progress [unlock-progress pick-angle layer-angles]
  (let [
        target-angle (target-angle unlock-progress layer-angles)
        ]
    (cond
      (= target-angle nil) unlock-progress
      (pick-matches pick-angle target-angle) (inc unlock-progress)
      :else 0
      )
    )
  )
