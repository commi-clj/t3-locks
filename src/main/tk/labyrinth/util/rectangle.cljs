(ns tk.labyrinth.util.rectangle)

(defn centre [rectangle]
  {
   :x (+ (:x rectangle) (/ (:width rectangle) 2))
   :y (+ (:y rectangle) (/ (:height rectangle) 2))
   }
  )

(defn rectangle [x y width height]
  {:x x :y y :width width :height height})
