(ns tk.labyrinth.util.ui-utils)

(defn element-centre [element]
  (let [
        box (.getBoundingClientRect element)
        ]
    {
     :x (+ (.-x box) (/ (.-width box) 2))
     :y (+ (.-y box) (/ (.-height box) 2))
     }
    )
  )
