(ns tk.labyrinth.util.math-js)

(defn atan2 [y x] (js/Math.atan2 y x))

(defn pi [] (.-PI js/Math))

(defn deg-to-rad [deg] (/ (* deg (pi)) 180))

(defn normalize-deg [deg]
  (let [rem (rem deg 360)]
    (if (>= rem 0) rem (+ rem 360))))

(defn angle-diff [first second]
  (let [raw-diff (normalize-deg (- first second))]
    (if (<= raw-diff 180) raw-diff (- 360 raw-diff))))

(defn projection-45 [] (/ Math/SQRT2 2))

(defn rad-to-deg [rad] (/ (* rad 180) (pi)))

(defn random [] (.random js/Math))

(defn random-between [from to]
  (let [
        range (- to from)
        ]
    (+ from (* (random) range))
    ))
