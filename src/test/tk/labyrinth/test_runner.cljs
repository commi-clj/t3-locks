(ns tk.labyrinth.test-runner
  (:require
    [doo.runner :refer-macros [doo-all-tests doo-tests]]
    [tk.labyrinth.lock.logic-test]
    [tk.labyrinth.util.math-js-test]
    [tk.labyrinth.util.rectangle-test]
    )
  )

(doo-tests
  'tk.labyrinth.lock.logic-test
  'tk.labyrinth.util.math-js-test
  'tk.labyrinth.util.rectangle-test
  )
