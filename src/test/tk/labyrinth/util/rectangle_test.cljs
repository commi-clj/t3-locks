(ns tk.labyrinth.util.rectangle-test
  (:require
    [cljs.test :refer-macros [deftest is testing run-tests]]
    [tk.labyrinth.util.rectangle :as rectangle]
    )
  )

(deftest centre
  (is (= {:x 0 :y 0} (rectangle/centre (rectangle/rectangle 0 0 0 0))))
  (is (= {:x 5 :y 10} (rectangle/centre (rectangle/rectangle 0 0 10 20))))
  (is (= {:x 4.5 :y 9.5} (rectangle/centre (rectangle/rectangle 0 0 9 19))))
  (is (= {:x -15 :y -30} (rectangle/centre (rectangle/rectangle -20 -40 10 20))))
  )

(deftest rectangle
  (is (= {:x 1 :y 2 :width 3 :height 4} (rectangle/rectangle 1 2 3 4)))
  )
