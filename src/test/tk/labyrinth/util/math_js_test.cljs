(ns tk.labyrinth.util.math-js-test
  (:require
    [cljs.test :refer-macros [deftest is testing run-tests]]
    [tk.labyrinth.util.math-js :as math]
    )
  )

(deftest angle-diff
  (is (= 0 (math/angle-diff 0 0)))
  (is (= 31 (math/angle-diff 0 31)))
  (is (= 31 (math/angle-diff 31 0)))
  (is (= 92 (math/angle-diff 12 1000)))
  (is (= 24 (math/angle-diff 12 -12)))
  )

(deftest deg-to-rad
  (is (= 0 (math/deg-to-rad 0)))
  (is (= 0.5235987755982988 (math/deg-to-rad 30)))
  (is (= 1.5707963267948966 (math/deg-to-rad 90)))
  (is (= 3.12413936106985 (math/deg-to-rad 179)))
  (is (= (math/pi) (math/deg-to-rad 180)))
  (is (= 3.159045946109736 (math/deg-to-rad 181)))
  (is (= 4.71238898038469 (math/deg-to-rad 270)))
  (is (= 6.457718232379019 (math/deg-to-rad 370)))
  (is (= -0.7853981633974483 (math/deg-to-rad -45)))
  )

(deftest normalize-deg
  (is (= 0 (math/normalize-deg 0)))
  (is (= 0 (math/normalize-deg 360)))
  (is (= 0 (math/normalize-deg 720)))
  (is (= 0 (math/normalize-deg -360)))
  (is (= 90 (math/normalize-deg 90)))
  (is (= 133 (math/normalize-deg 133)))
  (is (= 359 (math/normalize-deg -1)))
  )

(deftest pi (is (= 3.141592653589793 (math/pi))))

(deftest projection-45 (is (= 0.7071067811865476 (math/projection-45))))

(deftest rad-to-deg
  (is (= 0 (math/rad-to-deg 0)))
  (is (= 57.29577951308232 (math/rad-to-deg 1)))
  (is (= 114.59155902616465 (math/rad-to-deg 2)))
  (is (= 171.88733853924697 (math/rad-to-deg 3)))
  (is (= 180 (math/rad-to-deg (math/pi))))
  (is (= 229.1831180523293 (math/rad-to-deg 4)))
  (is (= 401.07045659157626 (math/rad-to-deg 7)))
  (is (= -57.29577951308232 (math/rad-to-deg -1)))
  )

(deftest random
  (let [values (map #(math/random) (range 100))]
    (is (= [] (filter #(or (< % 0) (>= % 1)) values))))
  )

(deftest random-between
  (let [from 0 to 1 values (map #(math/random-between from to) (range 100))]
    (is (= [] (filter #(or (< % from) (>= % to)) values))))
  (let [from 10 to 20 values (map #(math/random-between from to) (range 100))]
    (is (= [] (filter #(or (< % from) (>= % to)) values))))
  (let [from -5 to 5 values (map #(math/random-between from to) (range 100))]
    (is (= [] (filter #(or (< % from) (>= % to)) values))))
  )
