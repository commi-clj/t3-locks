(ns tk.labyrinth.lock.logic-test
  (:require
    [cljs.test :refer-macros [deftest is testing run-tests]]
    [tk.labyrinth.lock.logic :as logic]
    )
  )

(deftest pick-matches
  (is (= true (logic/pick-matches 0 0)))
  (is (= true (logic/pick-matches 0 9)))
  (is (= true (logic/pick-matches 9 0)))
  (is (= true (logic/pick-matches 0 10)))
  (is (= true (logic/pick-matches 10 0)))
  (is (= true (logic/pick-matches 5 -5)))
  (is (= false (logic/pick-matches 0 11)))
  (is (= false (logic/pick-matches 11 0)))
  )
